package com.arches.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;

import com.arches.CaystonArchesResponseGenerator.ArchesUserResponseApp;

public class ErrorLogger {

	String pathname;
	public ErrorLogger()
	{
		ResourceBundle resource=ResourceBundle.getBundle(ArchesUserResponseApp.configPropertiesLocation);
		this.pathname=resource.getString("errorLogsLocation")+"ARCHES_GILEAD_CAYSTON_CAP_ENROLLMENT_ERRORS_"+DateTime.getDateTime()+".txt";
		
	}
	public void writeErrorLogs(List<String> errors) throws IOException
	{ 
		errors.add(0,Calendar.getInstance(TimeZone.getTimeZone("EST")).getTime().toString());
		
		File file=new File(pathname);
		//true for append mode
		FileUtils.writeLines(file,errors,true);
    
	}
}
