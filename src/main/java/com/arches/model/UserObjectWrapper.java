package com.arches.model;

public class UserObjectWrapper {
UserDet userDet;
UserProfile userProfile;
public UserDet getUserDet() {
	return userDet;
}
public void setUserDet(UserDet userDet) {
	this.userDet = userDet;
}
public UserProfile getUserProfile() {
	return userProfile;
}
public void setUserProfile(UserProfile userProfile) {
	this.userProfile = userProfile;
}


}
