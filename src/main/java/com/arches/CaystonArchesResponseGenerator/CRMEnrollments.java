package com.arches.CaystonArchesResponseGenerator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;

import com.arches.model.UserDet;
import com.arches.model.UserObjectWrapper;
import com.arches.model.UserProfile;

public class CRMEnrollments {

	public List<UserObjectWrapper> getEnrollments()
	{
		List<UserObjectWrapper> newEnrollments=new ArrayList<UserObjectWrapper>();
		try{
			System.out.println("---------START EXTRACTING NEW ENORLLMENTS IN 24 HRS.------------------");
					
		Configuration cfg=new Configuration();
		cfg.configure(ArchesUserResponseApp.pathToHibernateConfigFile);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session=sessionFactory.openSession();
		//AND UD.capPatientId is NOT NULL deleted. Since users who register themselves on website won't have it
		String hql1="select UD from UserDet UD where  UD.password IS NOT NULL AND  UD.capPatientId IS NOT NULL  AND (((:currentTS-UD.createdOn)<=24*60*60*1000) or ((:currentTS-UD.modifiedOn)<=24*60*60*1000)))";
		
		String hql2="";
		String hql2_option1="select UP from UserProfile UP where userUid=:tempUDuuid AND modifiedOn=:tempUDmodifiedOn";
		String hql2_option2="select UP from UserProfile UP where userUid=:tempUDuuid AND modifiedOn is NULL";
		Query query1 = session.createQuery(hql1);
		long currentTS=System.currentTimeMillis();
		query1.setParameter("currentTS",new BigDecimal(Long.toString(currentTS)));
		//query1.setParameter("crmEnrollmentFlag",1);
		List<UserDet> results1=query1.list();
		Iterator<UserDet> results1Iterator=results1.iterator();
		
		while(results1Iterator.hasNext())
		{
			UserDet tempUserDetData=results1Iterator.next();
			System.out.println("UD data from DB:"+tempUserDetData.getEmail());
		
			if(tempUserDetData.getEmail().contains("@archestechnology.com") || tempUserDetData.getEmail().contains("@dkidirect.com") || tempUserDetData.getEmail().contains("@test.test") || tempUserDetData.getEmail().contains("@test.com") || tempUserDetData.getEmail().contains("@multiple.multiple"))
			{
				System.out.println("skip the internal arches record");
				continue;
			}
			
			if(tempUserDetData.getModifiedOn()==null)
			{
				System.out.println("modified date is null..proceeding with option 2");
				hql2=hql2_option2;
			}
			else
			{
				System.out.println("modified date is NOT null..proceeding with option 1");
				
				hql2=hql2_option1;
			}
			//get the recently modified userDet
			Query query2=session.createQuery(hql2);
			//use the uuid and modifiedOn values of UserDet to get single most recent value for each user from UP
		query2.setParameter("tempUDuuid",((UserDet)tempUserDetData).getUuid());
	
		if(tempUserDetData.getModifiedOn()!=null)
		{query2.setParameter("tempUDmodifiedOn",tempUserDetData.getModifiedOn());}
		
			List<UserProfile> results2=query2.list();
		Iterator<UserProfile> results2Iterator=results2.iterator();
		while(results2Iterator.hasNext())
		{
			
			//add UP and UD object to new enrollments map
			UserProfile tempUserProfileData=results2Iterator.next();
			System.out.println("UP data:"+tempUserProfileData.getEmail());
			
			UserObjectWrapper userObjectWrapper=new UserObjectWrapper();
			userObjectWrapper.setUserDet(tempUserDetData);
			userObjectWrapper.setUserProfile(tempUserProfileData);
				newEnrollments.add(userObjectWrapper);
		}
		}
		session.clear();
		session.close();
		System.out.println("----------RECENT ENROLLMENTS EXTRACTED. EXIT-------------");
		
		}
		catch(Exception e)
		{
			System.out.println("exception in crm enrollments:"+e);
			e.printStackTrace();
			ArchesUserResponseApp.errorLogs.add("Exception extracting recent enrollments from DB. Exception:"+e);
			   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		finally
		
		{
			System.out.println("returnining new enrollments:"+newEnrollments.size());
			return newEnrollments;

		}
		
	}
}
