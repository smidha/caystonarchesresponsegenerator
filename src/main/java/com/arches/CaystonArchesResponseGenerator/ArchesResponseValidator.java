package com.arches.CaystonArchesResponseGenerator;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.arches.model.UserDet;
import com.arches.model.UserProfile;
import com.arches.utilities.DateTime;
import com.google.gson.Gson;

public class ArchesResponseValidator {

	//method to validate fields in user object and return list of errors
	public List<String> validate(UserDet user,UserProfile userProfile) throws ParseException
	{
		List<String> errorsList=new ArrayList<String>();
		
	try
	{
	
		
		if((user.getFirstName().length()==0 || user.getFirstName().length()>50 || user.getLastName().length()>50 || user.getLastName().length()==0 || user.getEmail().length()==0))
		{
			errorsList.add("First name,Last name, Email are mandatory fields and cannot exceed 50 for user:"+new Gson().toJson(user));
		}
		
		if(user.getZip()!=null && (user.getZip().length()>0 && (user.getZip().length()>9 || !user.getZip().matches("[0-9]*"))))
		{
			errorsList.add("Invalid Zip code for user:"+new Gson().toJson(user));
		}
	if(user.getEmail()!=null && ((user.getEmail().length()>0 && user.getEmail().length()<4) || user.getEmail().length()>100 || !(user.getEmail().length()>0 && user.getEmail().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))))	
			
		{
			errorsList.add("invalid email address for user:"+new Gson().toJson(user));
		}
		if(user.getAddress1()!=null && (user.getAddress1().length()>0 && user.getAddress1().length()>100))
		{
			errorsList.add("invalid address1 length for user:"+new Gson().toJson(user));
		}
		if(user.getAddress2()!=null && (user.getAddress2().length()>0 && user.getAddress2().length()>100))
		{
			errorsList.add("invalid address2 length for user:"+new Gson().toJson(user));
		}
	
		if(user.getCity()!=null && user.getCity().length()>40)
		{
			errorsList.add("invalid city field length for user:"+new Gson().toJson(user));
		}
		if(user.getState()!=null && user.getState().length()>40)
		{
			errorsList.add("invalid state field length for user:"+new Gson().toJson(user));
		}
		if(user.getContactNumber()!=null && !user.getContactNumber().equals("") && (user.getContactNumber().length()!=10 || !user.getContactNumber().matches("[0-9]*")))
		{
			errorsList.add("invalid phone number for user:"+new Gson().toJson(user));
		}
		if(userProfile.getGender()==null || userProfile.getGender().length()!=1 || !(userProfile.getGender().equals("M") || userProfile.getGender().equals("F")))
		{
			errorsList.add("invalid gender value for user:"+new Gson().toJson(user));
		}
		
		
		
	
		    	
		    if(user.getCapPatientId()!=null && user.getCapPatientId().toString().length()==0)
		    {
		    	errorsList.add("Response required for CAYQ2 for user:"+new Gson().toJson(user));
		    }
		    	if(!(user.getBrandedOptCheck().toString().equals("0") || user.getBrandedOptCheck().toString().equals("1")))
		    {
		    	errorsList.add("Invalid Response for CAYQ15 for user:"+new Gson().toJson(user));
		    
		    }
		    	
		    	if(user.getOptInDate()!=null &&  user.getOptInDate().toString().length()>0 && (user.getOptInDate().toString().length()!=8 || DateTime.StringToDate(user.getOptInDate().toString()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptInDate().toString()).before(DateTime.StringToDate("20090101")))))
		    	{
		    		errorsList.add("invalid/missing response for CAYQ17 for user:"+new Gson().toJson(user));
		    	}
		     	if(user.getOptOutDate()!=null && user.getOptOutDate().toString().length()>0 && (user.getOptOutDate().toString().length()!=8 || DateTime.StringToDate(user.getOptOutDate().toString()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptOutDate().toString()).before(DateTime.StringToDate("20090101")))))
		    	{
		    		errorsList.add("invalid response for CAYQ18 for user:"+new Gson().toJson(user));
		    	}
		 
		    
		    	
	}
	catch(Exception e)
	{
		ArchesUserResponseApp.errorLogs.add("Exception in validating user object for arches response."+e);
		e.printStackTrace();
		   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
	}
	finally{
		return errorsList;
	}
	}
}
