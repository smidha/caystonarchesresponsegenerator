package com.arches.CaystonArchesResponseGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.io.FileUtils;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.io.ICsvWriter;
import org.supercsv.prefs.CsvPreference;

import com.arches.model.UserDet;
import com.arches.model.UserObjectWrapper;
import com.arches.model.UserProfile;
import com.arches.pgp.PGPTool;
import com.arches.utilities.DateTime;

public class ArchesUserResponseApp {

	
	public static String configPropertiesLocation;
	public static String pathToHibernateConfigFile;

	private static String archesFilePath="";
	private static String encryptedFilePath="";
	private static String responseErrorLogsPath="";
	private static long enrollments=0;
	public static Logger caystonResponseLogger;
	public static String caystonResponseLogLocation;
	

	public static List<String> errorLogs;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		configPropertiesLocation="config";
		setFilePaths();
		FileHandler fileHandler;
		try {
	  		fileHandler=new FileHandler(ArchesUserResponseApp.caystonResponseLogLocation+DateTime.getDateTime()+".txt",true);
	  		caystonResponseLogger.addHandler(fileHandler);
	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
	  		fileHandler.setFormatter(simpleFormatter);
	  	   ArchesUserResponseApp.caystonResponseLogger.log(Level.INFO,"Arches Response Logging for date:"+DateTime.getDateTime(),"Arches Response Logging for date:"+DateTime.getDateTime());
	  	}
	  	catch(Exception e)
	  	{
	  		System.out.println("Exception in configuring logger."+e);
	  		errorLogs.add("Exception in configuring logger in user response app."+e);
	 	   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
	  	}
		CRMEnrollments crmEnrollments=new CRMEnrollments();
		List<UserObjectWrapper> newEnrollments=crmEnrollments.getEnrollments();
		List<String> responseList=new ArrayList<String>();
		ArchesResponseValidator archesResponseValidator=new ArchesResponseValidator();
		errorLogs=new ArrayList<String>();
		if(newEnrollments==null || newEnrollments.isEmpty())
		{
			//dont call validator
			System.out.println("New enrollments is empty");
		}
		else
		{
			ArchesUserResponseApp.enrollments=newEnrollments.size();
		Iterator<UserObjectWrapper> listIterator=newEnrollments.iterator();
		while(listIterator.hasNext())
		{
			UserObjectWrapper userObj=listIterator.next();
			List<String> validationErrorsList = null;
			try {
				validationErrorsList = archesResponseValidator.validate(userObj.getUserDet(),userObj.getUserProfile());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
			}
		
			if(validationErrorsList==null || validationErrorsList.isEmpty() )
			{System.out.println("New enrollments is NOT empty and validations r ok");

				String response=formatResponseObject(userObj.getUserDet(),userObj.getUserProfile());
				responseList.add(response);
			}
			else
			{
				ArchesUserResponseApp.errorLogs.add("Validation errors present in generated response. "+validationErrorsList);
				System.out.println("validation errors:"+validationErrorsList+"-"+validationErrorsList.size());
				String response=formatResponseObject(userObj.getUserDet(),userObj.getUserProfile());
				responseList.add(response);
		
			}
		}
		
	}
		System.out.println("RESPONSE LIST:"+responseList);
		//call writing to response txt file
		generateResponseFile(responseList);
			
		try {
			writeErrorLogs(errorLogs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error in writing error logs. "+e);
			   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		for(Handler h:caystonResponseLogger.getHandlers())
		{
		    h.close();   //must call h.close or a .LCK file will remain.
		}
		System.exit(0);
	}

	public static  String formatResponseObject(UserDet user,UserProfile userProfile)
	{
	
		if(user.getState()!=null && user.getState().equals("State*"))
		{
			user.setState("".trim());
		}
		String brandedOptCheck=null;
		if(user.getBrandedOptCheck()==1)
		{
			brandedOptCheck="CAYQ15A1";
		}
		else if(user.getBrandedOptCheck()==0)
		{
			brandedOptCheck="CAYQ15A2";
		}
			
		if(userProfile.getDob()!=null && !userProfile.getDob().equals(""))
		{
		userProfile.setDob(DateTime.changeFormat(userProfile.getDob(),"yyyyMMdd", "yyyy-MM-dd"));
		}
		else
		{
			userProfile.setDob(null);
		}
		
		String response="\""+user.getFirstName()+"\","+"\""+user.getLastName()+"\","+"\""+user.getEmail()+"\",";
		response=response+"\""+user.getAddress1()+"\","+"\""+user.getAddress2()+"\","+"\""+user.getCity()+"\","+"\""+user.getState()+"\","+"\""+user.getZip()+"\",";
		response=response+"\""+user.getContactNumber()+"\","+"\""+userProfile.getGender()+"\","+"\""+userProfile.getDob()+"\",";
		response=response+"\""+user.getCapPatientId()+"\","+"\""+user.getUuid()+"\","+"\""+user.getMediaSourceId()+"\",";
		response=response+"\"CAYQ15~"+brandedOptCheck+"~~";
		response=response+"CAYQ17~"+user.getOptInDate()+"~~CAYQ18~"+user.getOptOutDate()+"\"";
		response=response.replaceAll("null","".trim());
		return response.trim();
	}
	
	public static void generateResponseFile(List<String> responseList)
	{
		System.out.println("received:"+responseList);
		String inputFilePath=archesFilePath+"ARCHES_GILEAD_CAYSTON_CRM_ENROLLMENT_"+DateTime.getDateTime()+"_"+responseList.size()+".txt";
		String outputFilePath=encryptedFilePath+"ARCHES_GILEAD_CAYSTON_CRM_ENROLLMENT_"+DateTime.getDateTime()+"_"+responseList.size()+".txt.pgp";
		//write to a text file all the records/lines
		Path file = Paths.get(inputFilePath);
		try 
		{
			
			Files.write(file,responseList, Charset.forName("UTF-8"));
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ArchesUserResponseApp.errorLogs.add("IO Exception in writing to ArchesUserResponse file."+e);
			System.out.println("exc in writing response");
			   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
			
		}
		ResourceBundle resource = ResourceBundle.getBundle(ArchesUserResponseApp.configPropertiesLocation);
		PGPTool pgpTool=new PGPTool();
		try {
			pgpTool.testEncrypt(inputFilePath,outputFilePath,resource.getString("PASSPHRASE"),resource.getString("CAREMETX_KEY"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ArchesUserResponseApp.errorLogs.add("Exception in encrypting ArchesUserResponse file."+e);
			System.out.println("exc in ecrypting response");
			e.printStackTrace();
			   ArchesUserResponseApp.caystonResponseLogger.log(Level.SEVERE,e.getMessage(),e);
			
		}
	}
	
		
	private static void setFilePaths()
	{
		ResourceBundle resource = ResourceBundle.getBundle(ArchesUserResponseApp.configPropertiesLocation);
		archesFilePath=resource.getString("archesResponseFileLocation");
		encryptedFilePath=resource.getString("encryptedResponseFileLocation");
		responseErrorLogsPath=resource.getString("responseErrorLogsLocation");
	      caystonResponseLogLocation=resource.getString("caystonETLLogLocation");
	      caystonResponseLogger=Logger.getLogger("caystonETLLogger");
	      pathToHibernateConfigFile=resource.getString("pathToHibernateFile");
	    
	}
	public static void writeErrorLogs(List<String> errors) throws IOException
	{ 
		String path=ArchesUserResponseApp.responseErrorLogsPath+"ARCHES_CAYSTON_CRM_ENROLLMENT_RESPONSE_"+DateTime.getDateTime()+"_"+ArchesUserResponseApp.enrollments+"_ERROR.txt";
		
		errors.add(0,Calendar.getInstance(TimeZone.getTimeZone("EST")).getTime().toString());
		
		File file=new File(path);
		//true for append mode
		FileUtils.writeLines(file,errors,true);
    
	}
}
